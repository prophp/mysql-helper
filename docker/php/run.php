<?php

require_once __DIR__ . "/bootstrap.php";

use ProPhp\CommandLine\CommandLine;

$defaultParametersFilePath = dirname(__DIR__) . "/default-params.txt";

$CommandLine = new CommandLine(
    file_exists($defaultParametersFilePath) ?
        array_merge(['scriptTitle'], CommandLine::stringToArgv(file_get_contents($defaultParametersFilePath))) :
        $argv
);

if (file_exists($defaultParametersFilePath)) {
    echo "NB! Default parameters used from file://$defaultParametersFilePath : '" . file_get_contents($defaultParametersFilePath) . "'\n";
}

$port = $CommandLine->getArgument(0) ?? 9999;

$dockerComposeContents = str_replace(
    '%port%',
    $port,
    file_get_contents(dirname(__DIR__) . "/templates/docker-compose.yml/php.txt")
);

$mysqlPort = null;

if ($CommandLine->optionIsSet('mysql')) {
    $dockerComposeContents .=
        PHP_EOL . file_get_contents(dirname(__DIR__) . "/templates/docker-compose.yml/php-mysql.txt");

    $mysqlPort = $CommandLine->getOptionValue('mysql-port') ?? $port - 1;

    $dockerComposeContents .=
        str_repeat(PHP_EOL, 2) . str_replace(
            '%port%',
            $mysqlPort,
            file_get_contents(dirname(__DIR__) . "/templates/docker-compose.yml/mysql.txt")
        );
}


file_put_contents(
    dirname(__DIR__, 2) . "/docker-compose.yml",
    $dockerComposeContents
);

# Start docker image
passthru("docker-compose up -d");

# Check that all requirements are present
$output = [];
exec("docker-compose exec server bash -c 'rsync --version'", $output);
$rsyncVersion = $output[0];

$output = [];
exec("docker-compose exec server bash -c 'php -v'", $output);

$phpVersion = $output[0];

echo "\n$rsyncVersion\n$phpVersion\n\n" .
    "Apache PHP: http://localhost:$port/\n/var/www/html/ = " . dirname(__DIR__, 2) . "\n";

if ($CommandLine->optionIsSet('mysql')) {
    echo "\nMySQL: http://localhost:$mysqlPort/\n";
}

echo "\n";

if (file_exists(dirname(__DIR__, 2) . "/.gitignore")) {

    $existingGitignoreContents = file_get_contents(dirname(__DIR__, 2) . "/.gitignore");

    if (strpos($existingGitignoreContents, "docker-compose.yml") === false) {
        file_put_contents(
            dirname(__DIR__, 2) . "/.gitignore",
            $existingGitignoreContents . PHP_EOL . "docker-compose.yml"
        );
    }

    exit;
}

file_put_contents(dirname(__DIR__, 2) . "/.gitignore", "/docker-compose.yml");

