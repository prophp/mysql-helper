README

### ◦ Requirements

- PHP 8:0+
- Linux environment (docker can be used)

### ◦ Start docker container (optional)

---
▸ shell
```shell
docker/run
```

Read more about `prophp/docker-bridge`

### ◦  Usage

#### Queries

---
▸ shell
```
bin/example.queries
```

---
Take a look at [example script](bin/php/example.queries.php)

#### Entities

---
▸ shell
```
bin/example.entities
```

---
Take a look at [example script](bin/php/example.entities.php)

#### Tables

---
▸ shell
```
bin/example.tables
```

---
Take a look at [example script](bin/php/example.tables.php)