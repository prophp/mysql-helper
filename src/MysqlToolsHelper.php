<?php

namespace ProPhp\MysqlTools;

class MysqlToolsHelper
{
    public function explodeFullSqlQuery(string $sql, string $subQueryDelimiter): array
    {
        $sqlQueries = explode($subQueryDelimiter, $sql);

        return array_filter($sqlQueries, function ($query) {
            return empty(trim($query)) === false;
        });
    }

    public function prepareValueString(mixed $value): string|int|float
    {
        if ($value === null) {
            $value = "NULL";
        } else {
            $value = is_numeric($value) ? $value : '"' . $this->escapeDoubleQuotes($value) . '"';
        }

        return $value;
    }

    private function escapeDoubleQuotes(string $string): string
    {
        return str_replace('"', '\"', $string);
    }

    public function prepareFieldsString(array $entityData)
    {
        $fields = array_keys($entityData);

        $fields = array_map(function ($value) {
            return "`$value`";
        }, $fields);

        return implode(", ", $fields);
    }

    public function populateSql(
        string &$sql, string $queryStringBeginning, array $valuesStrings, string $subQueryDelimiter = null
    )
    {
        $valuesString = implode(",\n", $valuesStrings);
        $queryStringBeginning .= "$valuesString\n;$subQueryDelimiter\n";
        $sql .= $queryStringBeginning;
    }

    public function initNewQueryData(
        array  &$buffer, array &$valuesStrings, string &$queryString,
        string $tableTitle, string $fieldTitlesString, string $dbPrefix
    )
    {
        $buffer[$tableTitle] = $fieldTitlesString;
        $valuesStrings = [];
        $queryString =
            "INSERT INTO\n$dbPrefix$tableTitle(" . $fieldTitlesString . ")\nVALUES\n";
    }

    public function arrayValuesForQueryString(array $array, bool $useQuotesForStrings = true)
    {
        $result = [];

        // @todo Use $this->prepareValueString()
        foreach (array_values($array) as $item) {
            $value = $item;
            if ($useQuotesForStrings) {
                $value = $item;
                if ($value === null) {
                    $value = "NULL";
                } else {
                    $value = is_numeric($item) ? $item : '"' . $this->escapeDoubleQuotes($item) . '"';
                }
            }
            $result[] = $value;
        }

        return $result;
    }
}

