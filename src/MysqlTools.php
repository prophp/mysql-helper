<?php

namespace ProPhp\MysqlTools;

class MysqlTools
{
    protected string $databaseName;

    private string $databasePrefix;

    private \PDO $pdo;

    private MysqlToolsHelper $tools;

    private MysqlToolsTables $tables;


    public function __construct(\PDO $pdo, string $databaseName, string $databasePrefix = '')
    {
        $this->pdo = $pdo;
        $this->databaseName = $databaseName;
        $this->setDatabasePrefix($databasePrefix);

        $this->tools = new MysqlToolsHelper();
    }

    /**
     * @return MysqlToolsTables
     *
     * Factory method
     */
    public function tables(): MysqlToolsTables
    {
        return new MysqlToolsTables($this->pdo, $this->databaseName, $this->databasePrefix);
    }

    public function setDatabasePrefix(string $databasePrefix)
    {
        $this->databasePrefix = $databasePrefix;
    }

    /**
     * @comment
     * This $subQueryDelimiter logic is required for a better ERROR handling. Statement returns true if the last subQuery was successful,
     * does not care about previous subQueries. Without $subQueryDelimiter SQL execution failures might stay uncaught
     */
    public function executeQueries(string $sql, string $subQueryDelimiter = null, bool $throwCustomException = true)
    {
        $sqlQueries = $subQueryDelimiter === null ? [$sql] : $this->tools->explodeFullSqlQuery($sql, $subQueryDelimiter);

        foreach ($sqlQueries as $sqlQuery) {
            $this->executeQuery($sqlQuery, $throwCustomException, false);
        }
    }

    public function executeQuery(string $sql, bool $throwCustomException = true, bool $returnStatement = false)
    {
        $statement = $this->pdo->prepare($sql);
        $success = $statement->execute();

        if (!$success && $throwCustomException) {
            list($sqlStateErrorCode, $driverSpecificErrorCode, $driverSpecificErrorMessage) = $statement->errorInfo();
            throw new \Exception("$sqlStateErrorCode: $driverSpecificErrorCode: $driverSpecificErrorMessage");
        }

        if ($returnStatement) {
            return $statement;
        }
    }

    public function fetchQuery(string $sql, bool $throwCustomException = true, int $mode = \PDO::FETCH_ASSOC)
    {
        return $this->executeQuery($sql, $throwCustomException, true)->fetchAll($mode);
    }

    public function deleteEntities(array $data, string $databasePrefix = null)
    {
        $databasePrefix = $databasePrefix ?? $this->databasePrefix;

        foreach ($data as $tableTitle => $entities) {
            foreach ($entities as $referenceData) {
                $keyValueReference = [];
                foreach ($referenceData as $columnTitle => $value) {
                    $keyValueReference[] = "`$columnTitle` = " . $this->tools->prepareValueString($value);
                }

                $sql = "DELETE FROM `{$databasePrefix}$tableTitle` WHERE " . implode(" AND ", $keyValueReference) . ";";
                $this->executeQuery($sql);
            }
        }
    }

    public function updateEntities(array $preparedData, string $databasePrefix = null)
    {
        $databasePrefix = $databasePrefix ?? $this->databasePrefix;

        foreach ($preparedData as $tableTitle => $entities) {
            foreach ($entities as $entityData) {
                $keyValueData = [];
                foreach ($entityData['data'] as $columnTitle => $value) {
                    $keyValueData[] = "`$columnTitle` = " . $this->tools->prepareValueString($value);
                }

                $keyValueReference = [];
                foreach ($entityData['reference'] as $columnTitle => $value) {
                    $keyValueReference[] = "`$columnTitle` = " . $this->tools->prepareValueString($value);
                }

                $sql = "UPDATE `{$databasePrefix}$tableTitle` " .
                    "SET " . implode(", ", $keyValueData) . " " .
                    "WHERE " . implode(" AND", $keyValueReference) . ";";
                $this->executeQuery($sql);
            }
        }
    }

    public function selectEntities()
    {

    }

    public function createEntities(array $data, string $databasePrefix = null)
    {
        $databasePrefix = $databasePrefix ?? $this->databasePrefix;

        $buffer = [];

        $subQueryDelimiter = "/* SUB-QUERY END */";
        $sql = "";

        foreach ($data as $tableTitle => $entities) {

            if (count($entities) === 0) {
                continue;
            }

            $entities = array_values($entities);

            $fieldTitlesString = $this->tools->prepareFieldsString($entities[0]);
            $queryString = "";
            $valuesStrings = [];

            $this->tools->initNewQueryData(
                $buffer, $valuesStrings, $queryString, $tableTitle, $fieldTitlesString, $databasePrefix
            );

            foreach ($entities as $entity) {

                $fieldTitlesString = $this->tools->prepareFieldsString($entity);

                // In case entity field amount or/and order has changed, need to start a new sub-query
                if ($fieldTitlesString !== $buffer[$tableTitle]) {
                    $this->tools->populateSql($sql, $queryString, $valuesStrings, $subQueryDelimiter);

                    $this->tools->initNewQueryData(
                        $buffer, $valuesStrings, $queryString, $tableTitle, $fieldTitlesString, $databasePrefix
                    );
                }

                $valuesStrings[] = "(" . implode(", ", $this->tools->arrayValuesForQueryString($entity)) . ")";
            }

            $this->tools->populateSql($sql, $queryString, $valuesStrings, $subQueryDelimiter);
        }

        $this->executeQueries($sql, $subQueryDelimiter, true);
    }

    function generateRelationshipSchema()
    {
        $foreignKeysData = array();

        // Get table names from database
        $sql = "SHOW TABLES";
        $tables = $this->pdo->query($sql)->fetchAll(\PDO::FETCH_COLUMN);

        foreach ($tables as $tableName) {
            $tableSchema = [];

            // Get columns for table
            $sql = "SHOW COLUMNS FROM $tableName";
            $columns = $this->pdo->query($sql)->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($columns as $column) {
                $columnName = $column['Field'];
                $columnType = $column['Type'];

                // Check if column has a foreign key constraint
                $sql = "SELECT REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE TABLE_NAME='$tableName' AND COLUMN_NAME='$columnName'";
                $fkResult = $this->pdo->query($sql)->fetch(\PDO::FETCH_ASSOC);
                /*
                            if ($columnName === 'address_id') {
                                var_dump([$tableName, $column, $fkResult]);
                            }
                */
                if ($fkResult) {
                    $referencedTable = $fkResult['REFERENCED_TABLE_NAME'];
                    $referencedColumnName = $fkResult['REFERENCED_COLUMN_NAME'];

                    if ($referencedTable !== null && $referencedColumnName !== null) {
                        // Add foreign key constraint to table schema
                        $tableSchema[$referencedTable] = [
                            "$tableName.column" => $columnName,
                            "$referencedTable.column" => $referencedColumnName
                        ];
                    }
                }
            }

            // Add table schema to JSON data
            if (!empty($tableSchema)) {
                $foreignKeysData[$tableName] = $tableSchema;
                continue;
            }

            $foreignKeysData[$tableName] = [];
        }

        $result = [];

        foreach ($foreignKeysData as $tableTitle => $foreignKeyData) {
            if (count($foreignKeyData) === 0) {
                $result[$tableTitle] = [];
                continue;
            }
            $result[$tableTitle]['many-to-one'] = $foreignKeyData;
            $foreignTableTitle = array_keys($foreignKeyData)[0];
            $result[$foreignTableTitle]['one-to-many'][$tableTitle] = $foreignKeyData;
        }

        return $result;
    }

    public function getTableColumns(string $tableTitle)
    {
        $columns = array_column($this->fetchQuery("SHOW COLUMNS FROM `$tableTitle`"), 'Field');

        return $columns;
    }

    private function prepareTableColumnsArray(string $tableTitle)
    {
        $result = [];

        foreach ($this->getTableColumns($tableTitle) as $column) {
            $result[] = "$tableTitle.`$column` AS `$tableTitle.$column`";
        }

        return $result;
    }

    private function getTablesFromRelationChains(array $relationChains)
    {
        $result = [];

        foreach ($relationChains as $relationChain) {
            $result[] = $relationChain['related_table_title'];
            if (isset($relationChain['related_table'])) {
                $result = array_merge($result, $this->getTablesFromRelationChains([$relationChain['related_table']]));
            }
        }

        return array_unique($result);
    }

    private function prepareColumnsString(string $columns, string $tableTitle, array $relationChains = [])
    {
        $columnsArray = [];

        if ($columns === '*') {
            if (count($relationChains) === 0) {
                // @todo Add '`...`' later
                return $columns;
            }


            $relationTables = $this->getTablesFromRelationChains($relationChains);

            $tables = array_merge([$tableTitle], $relationTables);

            foreach ($tables as $table) {
                $columnsArray = array_merge($columnsArray, $this->prepareTableColumnsArray($table));
            }

            return implode(", ", $columnsArray);
        }


        return '*';
    }

    public function generateSelectQuery(string $tableTitle, string $columns, array $where = [], array $relationSchema = [])
    {
        $relationColumns = array_filter(array_keys($where), function ($columnTitle) {
            return str_contains($columnTitle, ".");
        });

        $relationTables = array_map(function ($relationColumn) {
            return explode(".", $relationColumn)[0];
        }, $relationColumns);

        $relationChains = [];

        $RelationSchema = (new \ProPhp\MysqlTools\MysqlToolsRelationSchema(
            $relationSchema
        ));

        foreach ($relationTables as $relationTable) {
            $relationChains[] = $RelationSchema->findRelationChain($tableTitle, $relationTable);
        }

        $joins = [];

        foreach ($relationChains as $relationChain) {
            $joins = array_merge($joins, $this->generateJoins($relationChain));
        }

        $finalSqlItems = array_merge(
            [
                "SELECT {$this->prepareColumnsString($columns, $tableTitle, $relationChains)}",
                "FROM `$tableTitle`"
            ],
            array_unique($joins)
        );

        $whereResult = [];

        foreach ($where as $columnReference => $value) {
            if (str_contains($columnReference, ".")) {
                $explodedColumnReference = explode(".", $columnReference);
                $columnReference = $explodedColumnReference[0] . ".`" . $explodedColumnReference[1] . "`";
            } else {
                $columnReference = "`$columnReference`";
            }

            $whereResult[] = "$columnReference = " . $this->tools->prepareValueString($value);
        }

        $finalSql = implode(PHP_EOL, $finalSqlItems) . PHP_EOL;
        $finalSql .= "WHERE" . PHP_EOL;
        $finalSql .= implode(PHP_EOL . "AND ", $whereResult);

        return $finalSql;
    }

    public function select(string $tableTitle, string $columns, array $where = [], array $relationSchema = [])
    {
        return $this->fetchQuery($this->generateSelectQuery($tableTitle, $columns, $where, $relationSchema));
    }

    private function generateJoins(array $relationChain)
    {
        $result = [];

        $joinColumnsData = $relationChain;

        unset($joinColumnsData['related_table_title']);
        unset($joinColumnsData['relation_type']);
        unset($joinColumnsData['related_table']);

        $joinColumns = [];

        foreach ($joinColumnsData as $columnTableRaw => $columnTitle) {
            $joinColumns[] = explode(".", $columnTableRaw)[0] . ".`$columnTitle`";
        }

        sort($joinColumns);

        $relatedTable = $relationChain['related_table_title'];

        $result[] = "INNER JOIN `$relatedTable` ON {$joinColumns[0]} = {$joinColumns[1]}";

        if (!isset($relationChain['related_table'])) {
            return $result;
        }

        return array_merge($result, $this->generateJoins($relationChain['related_table']));
    }

}