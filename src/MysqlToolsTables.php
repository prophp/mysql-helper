<?php

namespace ProPhp\MysqlTools;

use ProPhp\Wildcard\Wildcard;

class MysqlToolsTables extends MysqlTools
{
    public function drop(array $includedWildcards = [], array $excludedWildcards = [])
    {
        $this->executeQuery("SET FOREIGN_KEY_CHECKS = 0");

        foreach ($this->list($includedWildcards, $excludedWildcards) as $tableTitle) {
            $this->executeQuery("DROP TABLE IF EXISTS `$tableTitle`");
        }

        $this->executeQuery("SET FOREIGN_KEY_CHECKS = 1");
    }

    public function list(array $includedWildcards = [], array $excludedWildcards = [])
    {
        $result = array_column($this->fetchQuery("SHOW TABLES"), "Tables_in_{$this->databaseName}");

        return Wildcard::filterIndexedArray($result, $includedWildcards, $excludedWildcards);
    }

    public function updatePrefixes(array $tableTitles, string $tablePrefixFrom, string $tablePrefixTo): void
    {
        $sql = "";
        foreach ($tableTitles as $table) {
            $sql .= "CREATE TABLE IF NOT EXISTS `$tablePrefixFrom$table` (`tmp` INT);/* SUB-QUERY DELIMITER */\n" .
                "RENAME TABLE `$tablePrefixFrom$table` TO `$tablePrefixTo$table`;/* SUB-QUERY DELIMITER */\n";
        }
        $this->executeQueries($sql, '/* SUB-QUERY DELIMITER */');
    }

    public function create(array $data)
    {

    }
}