<?php

namespace ProPhp\MysqlTools;

class MysqlToolsRelationSchema
{
    private array $schema;

    public function __construct(array $schema)
    {
        $this->schema = $schema;
    }

    private function findRelations(string $sourceTableTitle, string $targetTableTitle, string $initialTableTitle = null)
    {
        $initialTableTitle = $initialTableTitle ?? $sourceTableTitle;

        $result = [];

        if ($sourceTableTitle === $targetTableTitle) {
            return $result;
        }

        foreach ($this->schema[$sourceTableTitle] as $relationType => $relatedTablesData) {
            foreach ($relatedTablesData as $relatedTableTitle => $relatedTableData) {
                if ($relatedTableTitle === $initialTableTitle) {
                    continue;
                }
                $result[] = array_merge(
                    [
                        'related_table_title' => $relatedTableTitle,
                        'relation_type' => $relationType
                    ],
                    $relatedTableData,
                    [
                        'related_tables' =>
                            $this->findRelations($relatedTableTitle, $targetTableTitle, $initialTableTitle)
                    ]
                );
            }
        }

        return $result;
    }

    public function findRelationChain(string $sourceTableTitle, string $targetTableTitle)
    {
        $relations = $this->findRelations($sourceTableTitle, $targetTableTitle);

        return $this->filterRelations($relations, $targetTableTitle);
    }

    private function filterRelations(array $relations, string $targetTableTitle)
    {
        $result = [];

        foreach ($relations as $relation) {
            $contains = str_contains(json_encode($relation), "table_title\":\"$targetTableTitle\"");

            if ($contains) {
                if($relation['related_table_title'] === $targetTableTitle){
                    unset($relation['related_tables']);
                    $result[] = $relation;
                    continue;
                }
                $relation['related_table'] = $this->filterRelations($relation['related_tables'], $targetTableTitle);
                unset($relation['related_tables']);
                $result[] = $relation;
            }
        }

        return $result[0];
    }
}