<?php

require_once dirname(__DIR__, 2) . "/vendor/autoload.php";

use ProPhp\MysqlTools\MysqlTools;

/**
 * Initialize @PDO
 */
$pdo = require_once __DIR__ . '/initialize-pdo.php';

/**
 * Initialize @MysqlTools
 */
$mysql = new MysqlTools($pdo, 'database', '');

/**
 * Create structure schema
 * NB!!! There should be none one-to-one relationship in generated schema. MySQL does not support this approach
 *
 * @link https://stackoverflow.com/questions/2427501/one-to-one-relationship-in-mysql
 */
$mysql->executeQueries("
SET FOREIGN_KEY_CHECKS=0;
/* SUB-QUERY DELIMITER */
DROP TABLE IF EXISTS `prefix_zipcode`;
/* SUB-QUERY DELIMITER */
CREATE TABLE prefix_zipcode (
  id INT NOT NULL AUTO_INCREMENT,
  code VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
);
/* SUB-QUERY DELIMITER */
DROP TABLE IF EXISTS `prefix_city`;
/* SUB-QUERY DELIMITER */
CREATE TABLE prefix_city (
  id INT NOT NULL AUTO_INCREMENT,
  title VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
);
/* SUB-QUERY DELIMITER */
DROP TABLE IF EXISTS `prefix_address`;
/* SUB-QUERY DELIMITER */
CREATE TABLE prefix_address (
  id INT NOT NULL AUTO_INCREMENT,
  street_address VARCHAR(200) NOT NULL,
  city_id INT,
  zipcode_id INT,
  state VARCHAR(50) NOT NULL,
  country VARCHAR(50) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (city_id) REFERENCES prefix_city(id),
  FOREIGN KEY (zipcode_id) REFERENCES prefix_zipcode(id)
);
/* SUB-QUERY DELIMITER */
DROP TABLE IF EXISTS `prefix_order`;
/* SUB-QUERY DELIMITER */
CREATE TABLE prefix_order (
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id)
);
/* SUB-QUERY DELIMITER */
DROP TABLE IF EXISTS `prefix_customer`;
/* SUB-QUERY DELIMITER */
CREATE TABLE prefix_customer (
  id INT NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(50) NOT NULL,
  email VARCHAR(100) NOT NULL,
  phone_number VARCHAR(20),
  address_id INT, # UNIQUE is making foreign key query to fail. Address fields should be included in Customer entity
  order_id INT,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY (address_id) REFERENCES prefix_address(id),
  FOREIGN KEY (order_id) REFERENCES prefix_order(id)
);
/* SUB-QUERY DELIMITER */
SET FOREIGN_KEY_CHECKS=1;
", '/* SUB-QUERY DELIMITER */', true);


file_put_contents(dirname(__DIR__, 2) . "/tmp/relation-schema.json", json_encode(
    $mysql->generateRelationshipSchema(),
    JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE
));


/**
 * @MysqlTools::generateSelectQuery()
 */
$result = $mysql->generateSelectQuery(
    'prefix_customer',
    '*',
    [
        'prefix_city.title' => 'Tallinn',
        'prefix_zipcode.code' => '12345',
    ],
    $mysql->generateRelationshipSchema()
);
if (
    $result !== "SELECT prefix_customer.`id` AS `prefix_customer.id`, prefix_customer.`first_name` AS `prefix_customer.first_name`, prefix_customer.`last_name` AS `prefix_customer.last_name`, prefix_customer.`email` AS `prefix_customer.email`, prefix_customer.`phone_number` AS `prefix_customer.phone_number`, prefix_customer.`address_id` AS `prefix_customer.address_id`, prefix_customer.`order_id` AS `prefix_customer.order_id`, prefix_customer.`created_at` AS `prefix_customer.created_at`, prefix_address.`id` AS `prefix_address.id`, prefix_address.`street_address` AS `prefix_address.street_address`, prefix_address.`city_id` AS `prefix_address.city_id`, prefix_address.`zipcode_id` AS `prefix_address.zipcode_id`, prefix_address.`state` AS `prefix_address.state`, prefix_address.`country` AS `prefix_address.country`, prefix_city.`id` AS `prefix_city.id`, prefix_city.`title` AS `prefix_city.title`, prefix_zipcode.`id` AS `prefix_zipcode.id`, prefix_zipcode.`code` AS `prefix_zipcode.code`
FROM `prefix_customer`
INNER JOIN `prefix_address` ON prefix_address.`id` = prefix_customer.`address_id`
INNER JOIN `prefix_city` ON prefix_address.`city_id` = prefix_city.`id`
INNER JOIN `prefix_zipcode` ON prefix_address.`zipcode_id` = prefix_zipcode.`id`
WHERE
prefix_city.`title` = \"Tallinn\"
AND prefix_zipcode.`code` = 12345") {
    throw new Exception("MysqlTools::generateSelectQuery() failed: " . json_encode($result));
}


/**
 * Create entities
 */
$mysql->createEntities([
    'zipcode' => [
        [
            // id 1
            'code' => 12345,
        ],
    ],
    'city' => [
        [
            // id 1
            'title' => 'Tallinn',
        ],
    ],
    'address' => [
        [
            // id 1
            'street_address' => 'Viru tänav 1',
            'city_id' => 1,
            'zipcode_id' => 1,
            'state' => 'Harju county',
            'country' => 'Estonia',
        ]
    ],
    'order' => [
        [
            // id 1
        ]
    ],
    'customer' => [
        [
            // id 1
            'first_name' => 'Philipp',
            'last_name' => 'N',
            'email' => 'dev@philippn.com',
            'phone_number' => '+372 56565656',
            'address_id' => 1,
            'order_id' => 1,
        ]
    ]
], 'prefix_');


// "prefix_customer.created_at": "2023-02-24 15:32:03",
$expectedJson = '[
  {
    "prefix_customer.id": 1,
    "prefix_customer.first_name": "Philipp",
    "prefix_customer.last_name": "N",
    "prefix_customer.email": "dev@philippn.com",
    "prefix_customer.phone_number": "+372 56565656",
    "prefix_customer.address_id": 1,
    "prefix_customer.order_id": 1,
    "prefix_address.id": 1,
    "prefix_address.street_address": "Viru tänav 1",
    "prefix_address.city_id": 1,
    "prefix_address.zipcode_id": 1,
    "prefix_address.state": "Harju county",
    "prefix_address.country": "Estonia",
    "prefix_city.id": 1,
    "prefix_city.title": "Tallinn",
    "prefix_zipcode.id": 1,
    "prefix_zipcode.code": "12345"
  }
]';


$result = $mysql->select(
    'prefix_customer',
    '*',
    [
        'prefix_city.title' => 'Tallinn',
        'prefix_zipcode.code' => '12345',
    ],
    $mysql->generateRelationshipSchema()
);

unset($result[0]['prefix_customer.created_at']);

if (
    $result != json_decode($expectedJson, true)
) {
    throw new Exception("MysqlTools::select() failed: " . json_encode($result));
}


/**
 * Success message
 */
echo "success" . PHP_EOL;