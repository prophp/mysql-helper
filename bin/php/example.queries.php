<?php

require_once dirname(__DIR__, 2) . "/vendor/autoload.php";

use ProPhp\MysqlTools\MysqlTools;

/**
 * Initialize @PDO
 */
$pdo = require_once __DIR__ . '/initialize-pdo.php';

/**
 * Initialize @MysqlTools
 */
$mysql = new MysqlTools($pdo, 'database', '');


/**
 * @comment
 * This $subQueryDelimiter logic is required for a better ERROR handling. Statement returns true if the last subQuery was successful,
 * does not care about previous subQueries. Without $subQueryDelimiter SQL execution failures might stay uncaught
 *
 * @MysqlTools::executeQueries()
 */
$mysql->executeQueries("
DROP TABLE IF EXISTS `test__table`;
/* SUB-QUERY DELIMITER */
CREATE TABLE `test__table` (
    `id` int unsigned NOT NULL AUTO_INCREMENT, 
    `name` varchar(255), 
    `second_name` varchar(255), 
    PRIMARY KEY (`id`)
);
/* SUB-QUERY DELIMITER */
INSERT INTO `test__table` (`name`, `second_name`) 
VALUES 
    ('Philipp', 'N'), 
    ('John', 'Doe');
", '/* SUB-QUERY DELIMITER */', true);


/**
 * @MysqlTools::fetchQuery()
 */
$result = $mysql->fetchQuery("SELECT * FROM `test__table`");
if (
    $result !== [
        ['id' => 1, 'name' => 'Philipp', 'second_name' => 'N'],
        ['id' => 2, 'name' => 'John', 'second_name' => 'Doe'],
    ]) {
    throw new Exception("MysqlTools::fetchQuery() or @MysqlTools::executeQueries() failed: " . json_encode($result));
}


/**
 * Success message
 */
echo "success" . PHP_EOL;