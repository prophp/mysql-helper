<?php

require_once dirname(__DIR__, 2) . "/vendor/autoload.php";

use ProPhp\MysqlTools\MysqlTools;

/**
 * Initialize @PDO
 */
$pdo = require_once __DIR__ . '/initialize-pdo.php';

/**
 * Initialize @MysqlTools
 */
$mysql = new MysqlTools($pdo, 'database', '');

$mysql->executeQueries("
DROP TABLE IF EXISTS `test__table`;
/* SUB-QUERY DELIMITER */
CREATE TABLE `test__table` (
    `id` int unsigned NOT NULL AUTO_INCREMENT, 
    `name` varchar(255), 
    `second_name` varchar(255), 
    PRIMARY KEY (`id`)
);
", '/* SUB-QUERY DELIMITER */', true);


/**
 * @MysqlTools::createEntities()
 */
$mysql->createEntities([
    'table' => [
        [
            'name' => 'Philipp',
            'second_name' => 'N',
        ],
        [
            'name' => 'John',
            'second_name' => 'Doe',
        ]
    ]
], 'test__');
$result = $mysql->fetchQuery("SELECT * FROM `test__table`");
if (
    $result !== [
        ['id' => 1, 'name' => 'Philipp', 'second_name' => 'N'],
        ['id' => 2, 'name' => 'John', 'second_name' => 'Doe'],
    ]) {
    throw new Exception("MysqlTools::createEntities() failed: " . json_encode($result));
}


/**
 * @MysqlTools::updateEntities()
 */
$mysql->updateEntities([
    'table' => [
        [
            'reference' => [
                'name' => 'John',
                'second_name' => 'Doe',
            ],
            'data' => [
                'second_name' => 'Doe Junior',
            ],
        ]
    ]
], 'test__');
$result = $mysql->fetchQuery("SELECT * FROM `test__table`");
if (
    $result !== [
        ['id' => 1, 'name' => 'Philipp', 'second_name' => 'N'],
        ['id' => 2, 'name' => 'John', 'second_name' => 'Doe Junior'],
    ]) {
    throw new Exception("MysqlTools::updateEntities() failed: " . json_encode($result));
}


/**
 * @MysqlTools::deleteEntities()
 */
$mysql->deleteEntities([
    'table' => [
        [
            'name' => 'John',
            'second_name' => 'Doe Junior',
        ],
    ]
], 'test__');
$result = $mysql->fetchQuery("SELECT * FROM `test__table`");
if (
    $result !== [
        ['id' => 1, 'name' => 'Philipp', 'second_name' => 'N'],
    ]) {
    throw new Exception("MysqlTools::deleteEntities() failed: " . json_encode($result));
}


/**
 * @MysqlTools::deleteEntities() $databasePrefix
 */
$mysql->setDatabasePrefix("test__");
$mysql->deleteEntities([
    'table' => [
        [
            'name' => 'Philipp',
            'second_name' => 'N',
        ],
    ]
]/*, null*/);
$result = $mysql->fetchQuery("SELECT * FROM `test__table`");
if ($result !== []) {
    throw new Exception("\$databasePrefix param works incorrectly: " . json_encode($result));
}


/**
 * Success message
 */
echo "success" . PHP_EOL;