<?php

require_once dirname(__DIR__, 2) . "/vendor/autoload.php";

use ProPhp\MysqlTools\MysqlTools;

/**
 * Initialize @PDO
 */
$pdo = require_once __DIR__ . '/initialize-pdo.php';

/**
 * Initialize @MysqlTools
 */
$mysql = new MysqlTools($pdo, 'database', '');


/**
 * @MysqlToolsTables::dropAll()
 */
$mysql->tables()->drop();
$result = $mysql->tables()->list();
if ($result !== []) {
    throw new Exception("@MysqlToolsTables::dropAll() or @MysqlToolsTables::list() failed: " . json_encode($result));
}


/**
 * @MysqlTools::executeQueries()
 */
$mysql->executeQueries("
DROP TABLE IF EXISTS `test__table`;
/* SUB-QUERY DELIMITER */
CREATE TABLE `test__table` (
    `id` int unsigned NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (`id`)
);
/* SUB-QUERY DELIMITER */
DROP TABLE IF EXISTS `test__table2`;
/* SUB-QUERY DELIMITER */
CREATE TABLE `test__table2` (
    `id` int unsigned NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (`id`)
);
", '/* SUB-QUERY DELIMITER */', true);


/**
 * @MysqlToolsTables::list()
 */
$result = $mysql->tables()->list();
if ($result !== ['test__table', 'test__table2']) {
    throw new Exception("@MysqlTools::executeQueries() or @MysqlToolsTables::list() failed: " . json_encode($result));
}


/**
 * @MysqlToolsTables::updatePrefixes()
 */
$mysql->tables()->updatePrefixes(['table', 'table2',], 'test__', 'test2__');
$result = $mysql->tables()->list();
if ($result !== ['test2__table', 'test2__table2']) {
    throw new Exception("@MysqlToolsTables::updatePrefixes() failed: " . json_encode($result));
}


/**
 * @MysqlTools::executeQueries()
 */
$mysql->executeQueries("
DROP TABLE IF EXISTS `test3__table`;
/* SUB-QUERY DELIMITER */
CREATE TABLE `test3__table` (
    `id` int unsigned NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (`id`)
);
", '/* SUB-QUERY DELIMITER */', true);
$result = $mysql->tables()->list();
if ($result !== ['test2__table', 'test2__table2', 'test3__table']) {
    throw new Exception("@MysqlTools::executeQueries() or @MysqlToolsTables::list() failed: " . json_encode($result));
}


/**
 * @MysqlToolsTables::drop()
 */
$mysql->tables()->drop(['test3*']);
$result = $mysql->tables()->list();
if ($result !== ['test2__table', 'test2__table2',]) {
    throw new Exception("@MysqlToolsTables::dropAll() or @MysqlToolsTables::list() failed: " . json_encode($result));
}


/**
 * Success message
 */
echo "success" . PHP_EOL;
